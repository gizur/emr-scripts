var hive = require('thrift-hive');

// Client connection
var client = hive.createClient({
  version: '0.7.1-cdh3u2',
  server: 'localhost',
  port: 9999,
  timeout: 1000
});

console.log('Before client.execute...');

// Execute call
client.execute('use default', function(err){

  console.log('Before client.query...');

  // Query call
  client.query('show tables')
  .on('row', function(database){
    console.log(database);
  })
  .on('error', function(err){
    console.log(err.message);
    client.end();
  })
  .on('end', function(){
    client.end();
  });


});

console.log('After client.execute...');
